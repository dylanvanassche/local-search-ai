#!/usr/bin/env python3
import argparse
import multiprocessing
import os.path
import signal
import random
import logging
import coloredlogs
import time
from parsers import InputParser, OutputParser
from initial_solution import InitialSolution
from benchmark import Benchmark
from simulated_annealing import SimulatedAnnealing
FACTOR = 1000

class LocalSearch:
    """
        Main class to execute a local search algorithm for the Cambio Car Sharing problem.
        Written by Anton Peeters, Jonas Michiels and Dylan Van Assche for the lab course 'Artificiele
        Intelligentie', Campus De Nayer (KU Leuven).
    """
    def __init__(self, input_file, output_file, timeout, seed, process_number):
        self._input_file = input_file
        self._output_file = output_file
        self._input_parser = InputParser(self._input_file)
        self._output_parser = OutputParser(self._output_file)
        self._seed = seed
        self._zone_list = None
        self._car_list = None
        self._request_list = None
        self._time_matrix = None
        self._days = None
        self._logger = logging.getLogger(self.__class__.__name__)
        self._benchmark = Benchmark("log{}.csv".format(process_number))
        self._timeout = timeout
        self._process_number = process_number
        self._SA = SimulatedAnnealing(0.995)
        # Read input file
        self._zone_list, self._car_list, self._request_list, self._days, self._time_matrix = self._input_parser.read()

    """Local search algorithm"""
    def search(self):
        # Get an initial solution for the local search algorithm
        random.seed(self._seed * self._process_number * FACTOR)
        self._logger.info("Creating initial solution...")
        solution = InitialSolution(self._time_matrix).generateRandomSolution(self._car_list,
                                                            self._zone_list,
                                                            self._request_list)
        self._logger.info(solution)
        initcost = solution.cost
        self._SA.init_temp(initcost * timeout)
        # Run local search algorithm
        self._logger.info("Starting local search algorithm...")
        iteration_counter = 0
        self._benchmark.log_iterations(iteration_counter, solution.cost)
        while True:
            if self._logger.level <= logging.DEBUG:
                iteration_counter += 1
                self._logger.info("Iteration: {}".format(iteration_counter))
                self._logger.debug("-" * 100)

            # Pick randomly 2 cars and switch them
            method_choice = random.random()

            if method_choice < 0.33:
                car1 = random.choice(solution._cars)
                car2 = car1
                while(car1 is car2):
                    car2 = random.choice(solution._cars)
                new_solution = solution.switch_cars(car1, car2)

            #Pick random car (where multiple cars in one zone) and move it
            elif method_choice >= 0.33 and method_choice < 0.66:
                car = self.select_car()
                new_solution = solution.move_car(car)

            elif method_choice >= 0.66 and method_choice < 1.0:
                new_solution = solution.switch_cars_advanced(11, 5)
            elif method_choice >= 0.5:
                req = random.choice(self._request_list)
                new_solution = solution.move_request_to_other_car(req)
            else:
                req = random.choice(self._request_list)
                new_solution = solution.move_request_to_other_zone(req, req.requested_zone)

            # Simulated annealing
            self._SA.decrement_temp()
            if(self._SA.accept_solution(solution.cost, new_solution.cost)):
                # Write solution to file since this is the best solution we found until now
                if (new_solution.cost < solution.cost):
                    unassigned_requests = []
                    for r in new_solution.requests:
                        if not r.is_assigned():
                            unassigned_requests.append(r)
                    self._benchmark.log_iterations(iteration_counter, new_solution.cost)
                    self._output_parser.write(new_solution.cost, new_solution.zones, new_solution.cars, unassigned_requests)

                # Use this new_solution to search further
                solution = new_solution
                self._logger.info("Found a better solution: {} ".format(new_solution))


            # Accept solution if the cost is lower than our current one
            #if new_solution.cost <= solution.cost:
                # Write solution to file since this is the best solution we found until now
                #unassigned_requests = []
                #for r in new_solution.requests:
                #    if not r.is_assigned():
                #        unassigned_requests.append(r)
                #self._benchmark.log_iterations(iteration_counter, new_solution.cost)
                #self._output_parser.write(new_solution.cost, new_solution.zones, new_solution.cars, unassigned_requests)

                # Use this new_solution to search further
                #solution = new_solution
                #self._logger.info("Found a better solution: {} ".format(new_solution))

        # We finished searching before an timeout occurred, exit
        self._logger.info("Search terminated because we're finished searching")
        exit(0)

    def select_car(self):
        while(True):
            selected_zone = random.choice(self._zone_list)
            if(len(selected_zone._assigned_cars)>0):
                break
        selected_car = random.choice(selected_zone._assigned_cars)
        return selected_car

# Start main program
if __name__ == "__main__":
    # Read arguments
    parser = argparse.ArgumentParser(description="A local search algorithm for the Cambio Car\
                                     Sharing problem")
    parser.add_argument("input", metavar="input", type=str, help="Input data file,\
                        absolute path.")
    parser.add_argument("output", metavar="output", type=str, help="Output data file,\
                        absolute path.")
    parser.add_argument("timeout", metavar="timeout", type=int, help="The maximum amount of time in\
                        seconds that the algorithm is allowed to search for an optimal solution.")
    parser.add_argument("seed", metavar="seed", type=int, help="The random seed value.")
    parser.add_argument("threads", metavar="number_of_thread", type=int, help="The amount of\
                        threads.")
    parser.add_argument("--verbose", "-v", action="store_true", help="Enable verbose logging.")
    args = parser.parse_args()
    input_file = args.input
    output_file = args.output
    timeout = args.timeout
    random_seed = args.seed
    number_of_processes = args.threads

    # Setup logging
    if(args.verbose):
        coloredlogs.install(level=logging.DEBUG)
    else:
        coloredlogs.install(level=logging.INFO)

    # Log input data
    logging.info("Input file path: {0}".format(input_file))
    logging.info("Output file path: {0}".format(output_file))
    logging.info("Timeout: {0}".format(timeout))

    # Check if timeout is > 0
    if timeout <= 0:
        parser.error("Timeout must be greater than 0.")

    # Check if files exists
    if os.path.isfile(input_file):
        process_list = []
        for p in range(0, number_of_processes):
            output_file_process = output_file.replace(".csv", str(p) + ".csv")
            ls = LocalSearch(input_file, output_file_process, timeout,
                    random_seed, p)
            process = multiprocessing.Process(target=ls.search)
            process.start()
            process_list.append(process)

        # Sleep until we reach the timeout or if the algorithm is finished
        for t in range(0, timeout):
            all_finished = False
            # If all processes are finished, break out the sleep loop
            for p in process_list:
                if p.is_alive():
                    all_finished = False
                    break
                else:
                    all_finished = True

            if all_finished:
                break

            # Sleep further...
            time.sleep(1)
        logging.info("Search finished")

        # Kill algorithm processes if still searching after timeout
        for p in process_list:
            if p.is_alive():
                logging.info("Search is still running, but we received a timeout, terminating...")
                p.terminate()
                p.join()
                logging.info("Termination successfully!")
    else:
        parser.error("Input file doesn't exist, please verify your input file path.")
        exit(1)
