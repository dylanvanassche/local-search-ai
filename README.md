# local-search-ai
Local Search algorithm to optimise car sharing locations for Cambio

## Installation
- `pipenv install`
- `./main.py <input_file> <output_file> <timeout_in_seconds> <random_seed> <number_of_threads>`

