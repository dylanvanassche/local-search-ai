#!/usr/bin/env python3
import math
import random


class SimulatedAnnealing:
    def __init__(self, decrement_factor):
        self._decrement_factor = decrement_factor

    def init_temp(self, temp):
        self.ctemp = temp

    def decrement_temp(self):
        self.ctemp = self._decrement_factor * self.ctemp

    def accept_solution(self, cost_old, cost_new):
        if cost_new <= cost_old:
            return True

        prob = math.exp((cost_old - cost_new) / self.ctemp)
        rf = random.random()
        print(str(prob) + "\n" + str(rf) + "\n" + "\n")
        return rf < prob
