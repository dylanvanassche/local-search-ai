#!/usr/bin/env python3
import logging
from car import Car


class Zone:
    """A Zone is a part of city with it's own ID and assigned cars"""
    def __init__(self, name, neighbours):
        self._name = name
        self._neighbours = neighbours
        self._assigned_cars = []
        self._logger = logging.getLogger(self.__class__.__name__)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return "ZONE {}\
                \n\tneighbours: {}\
                \n\tassigned cars: {}"\
                .format(self._name,
                        self._neighbours,
                        self._assigned_cars)

    """Add a car to a Zone"""
    def add_car(self, car):
        assert(isinstance(car, Car))
        self._assigned_cars.append(car)

    """Remove a car to a Zone"""
    def remove_car(self, car):
        assert(isinstance(car, Car))
        self._assigned_cars.remove(car)

    @property
    def name(self):
        return self._name

    @property
    def neighbours(self):
        return self._neighbours

    @neighbours.setter
    def neighbours(self, n):
        self._neighbours = n

    @property
    def assigned_cars(self):
        return self._assigned_cars
