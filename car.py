#!/usr/bin/env python3
from request import Request


class Car:
    """A Car contains all the assigened requests and a name of the car"""
    def __init__(self, name):
        self._name = name
        self._assigned_requests = []
        self._assigned_zone = None

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return "CAR {}\
                \n\tassigned requests: {}"\
                .format(self._name, self._assigned_requests)

    """Add a request to this Car"""
    def add_request(self, request):
        assert(isinstance(request, Request))
        self._assigned_requests.append(request)

    """Remove a request of this Car"""
    def remove_request(self, request):
        assert(isinstance(request, Request))
        self._assigned_requests.remove(request)

    @property
    def name(self):
        return self._name

    @property
    def assigned_requests(self):
        return self._assigned_requests

    @assigned_requests.setter
    def assigned_requests(self, value):
        self._assigned_requests = value

    @property
    def assigned_zone(self):
        return self._assigned_zone

    @assigned_zone.setter
    def assigned_zone(self, value):
        self._assigned_zone = value
