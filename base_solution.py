#!/usr/bin/env python3
import logging

class BaseSolution:
    def __init__(self, time_matrix):
        self._time_matrix = time_matrix
        self._logger = logging.getLogger(self.__class__.__name__)

    def check_overlap(self, req, car):
        for assigned_req in car.assigned_requests:
            if self._time_matrix.is_overlapping(req.name, assigned_req.name):
                self._logger.debug("Overlap found between request {} and request {} for car {}"\
                                   .format(req.name, assigned_req.name, car.name))
                return True
        return False

    def assign_request(self, req, car):
        is_assigned = False
        try:
            req.assign(car.assigned_zone)
            car.assigned_requests.append(req)
            self._logger.debug("Request {} successfully assigned to car {}"\
                               .format(req.name, car.name))
            is_assigned = True
        except ValueError:
            pass
        return is_assigned

    def reassign_requests(self, requests):
        for req in requests:
            # Keep a copy of the requests cars since we will manipulate this list
            car_list = req.allowed_cars.copy()
            is_assigned = False

            # Assigned requests are skipped
            if req.is_assigned():
                self._logger.debug("Request {} is already assigned, skipping...".format(req.name))
                continue

            # Try to assign the request as long as the request is unassigned and we still have some
            # allowed cars available.
            while not req.is_assigned() and car_list:
                car = self._random.choice(car_list)
                car_list.remove(car)
                is_assigned = False

                # Check overlap
                is_overlapping = self.check_overlap(req, car)

                # Try to assign the request if no overlap was found
                if not is_overlapping:
                    self._logger.debug("Trying to assign request {} since we haven't found any overlap for car {}".format(req.name, car.name))
                    is_assigned = self.assign_request(req, car)

                # Breaking out of the loop since we assigned our request already
                if is_assigned:
                    break

            # We couldn't assign our request in the loop, we mark the request as unassigned
            if not is_assigned:
                self._logger.debug("Unable to assign request {}".format(req.name))
                req.unassign()
