#!/usr/bin/env python3
import datetime


class Benchmark:
    def __init__(self, path):
        self._path = path
        # Clear any previous files
        with open(self._path, 'w'):
            pass

    def log_iterations(self, iteration, cost):
        line = str(datetime.datetime.now().time()) + "," + str(iteration) + "," + str(cost) + "\n"
        with open(self._path, "a") as f:
            f.write(line)

    def close(self):
        self.f.close()
