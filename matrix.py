#!/usr/bin/env python3
import numpy
import logging


class Matrix:
    def __init__(self, number_of_requests):
        # Python lists are less efficient
        self._matrix = numpy.full((number_of_requests, number_of_requests), True)
        self._logger = logging.getLogger(self.__class__.__name__)

    def calculate(self, start, stop):
        for i in range(0, len(self._matrix)):
            for j in range(0, len(self._matrix)):
                if(i == j):
                    self._matrix[i][j] = False
                elif(stop[i] <= start[j] and start[i] <= start[j]):
                    self._matrix[i][j] = False
                elif(stop[j] <= start[i] and start[j] <= start[i]):
                    self._matrix[i][j] = False

    def is_overlapping(self, request1, request2):
        # Read both requests and see if they overlap
        return self._matrix[request1][request2]

    def __str__(self):
        x = [""]
        for i in range(0, len(self._matrix)):
            for j in range(0, len(self._matrix)):
                if(self._matrix[i][j]):
                    x.append(str(1))
                else:
                    x.append(str(0))
            x.append("\n")
        return " ".join(x)

    @property
    def matrix(self):
        return self._matrix
