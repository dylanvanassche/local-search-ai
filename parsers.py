#!/usr/bin/env python3
import logging
from car import Car
from zone import Zone
from request import Request
from matrix import Matrix


class BaseParser:
    """
        Base class for all parser. This class provides a read file method which can be used in all
        the parsers.
    """
    def __init__(self, path):
        self._path = path
        self._logger = logging.getLogger(self.__class__.__name__)

    def _print_lines(self, lines):
        for line in lines:
            self._logger.info(line.strip())

    @property
    def path(self):
        return self._path


class InputParser(BaseParser):
    """Input parser, based on BaseParser"""
    def __init__(self, path):
        super().__init__(path)
        self._logger = logging.getLogger(self.__class__.__name__)

    """Read the file"""
    def read(self):
        # Read the file
        lines = []
        with open(self._path, "r") as f:
            while True:
                line = f.readline()
                if not line:
                    break
                lines.append(line.strip())
                if line.startswith("+"):
                    start = line[1:].split(":")
                    types = start[0]
                    amount = int(start[1])

                    # Parse requests
                    if types == "Requests":
                        time_matrix = Matrix(amount)
                        request_start_matrix = []
                        request_stop_matrix = []
                        request_list = []
                        for i in range(amount):
                            item = f.readline().strip().split(";")

                            # Parse each string
                            request_number = int(item[0].split("req")[1])
                            zone_number = int(item[1].split("z")[1])
                            car_numbers = item[5].split(",")
                            for index, car in enumerate(car_numbers):
                                car_numbers[index] = int(car.split("car")[1])
                            penalty_unassigned = int(item[6])
                            penalty_neighbor = int(item[7])
                            matrix_start = int(item[2])*24*60 + int(item[3])
                            matrix_end = int(item[2])*24*60 + int(item[3]) + int(item[4])

                            # Create Request object
                            req = Request(request_number,
                                          zone_number,
                                          car_numbers,
                                          penalty_unassigned,
                                          penalty_neighbor)

                            # Save it to the lists
                            request_list.append(req)
                            request_start_matrix.append(matrix_start)
                            request_stop_matrix.append(matrix_end)

                    # Parse zones
                    elif types == "Zones":
                        zone_list = []
                        for i in range(amount):
                            item = f.readline().strip().split(";")

                            # Parse each string
                            zone_number = int(item[0].split("z")[1])
                            neighbor_numbers = item[1].split(",")
                            for index, neighbor in enumerate(neighbor_numbers):
                                neighbor_numbers[index] = int(neighbor.split("z")[1])

                            # Create Zone object and save it
                            z = Zone(zone_number, neighbor_numbers)
                            zone_list.append(z)

                        # Convert neighbor zone numbers to Zone objects
                        for zone in zone_list:
                            replacement_list = []
                            for index, neighbor in enumerate(zone.neighbours):
                                for replacement in zone_list:
                                    if replacement.name == neighbor:
                                        replacement_list.append(replacement)
                                        break
                            zone.neighbours = replacement_list

                        # Convert requested zone numbers to Zone objects
                        for request in request_list:
                            for zone in zone_list:
                                if zone.name == request.requested_zone:
                                    request.requested_zone = zone
                                    break

                    # Parse vehicles
                    elif types == "Vehicles":
                        car_list = []
                        for i in range(amount):
                            item = f.readline().strip()
                            car_number = int(item.split("car")[1])

                            # Create Car object and save it
                            c = Car(car_number)
                            car_list.append(c)

                        # Convert allowed car numbers to Car objects
                        for request in request_list:
                            replacement_list = []
                            for index, car in enumerate(request.allowed_cars):
                                for replacement in car_list:
                                    if replacement.name == car:
                                        replacement_list.append(replacement)
                                        break
                            request.allowed_cars = replacement_list

                    # Parse days
                    elif types == "Days":
                        days = amount

                    # Generate error when an unknown type has been found in the input file
                    else:
                        raise TypeError("Unable to parse input file, unknown type: %s" % types)

        # Calculate Time Matrix
        time_matrix.calculate(request_start_matrix, request_stop_matrix)

        self._print_lines(lines)
        self._logger.debug("Generated time matrix:\n{}".format(time_matrix))

        return zone_list, car_list, request_list, days, time_matrix


class OutputParser(BaseParser):
    """Output parser, based on BaseParser"""
    def __init__(self, path):
        super().__init__(path)
        self._logger = logging.getLogger(self.__class__.__name__)

    """Write the file"""
    def write(self, cost, zones, cars, unassigned_requests):
        # Write the file
        with open(self._path, "w") as f:
            # Write cost
            f.write(str(cost)+"\n")

            # Write all zones
            f.write("+Vehicle assignments\n")
            for z in zones:
                for c in z.assigned_cars:
                    f.write("car{};z{}\n".format(c.name, z.name))

            # Write all cars
            f.write("+Assigned requests\n")
            for c in cars:
                for r in c.assigned_requests:
                    f.write("req{};car{}\n".format(r.name, c.name))

            # Write all unassigned_requests
            f.write("+Unassigned requests\n")
            for u in unassigned_requests:
                f.write("req{}\n".format(u.name))

        # Print debug information if `-O` flag isn't set
        if self._logger.level >= logging.INFO:
            self._logger.info("Output file written:")
            with open(self._path) as f:
                lines = f.readlines()
            self._print_lines(lines)
