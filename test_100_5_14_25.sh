#!/bin/bash
NUMBER_OF_PROCESSES=$(nproc)
rm 100_5_14_25_out*
echo "Local Search:"
echo "==============="
python3 main.py 100_5_14_25.csv 100_5_14_25_out.csv 60 1563535  $NUMBER_OF_PROCESSES 

echo "----------------------------------------------------------------------------------"
echo "Validator:"
echo "============"

STOP_VALUE=$((NUMBER_OF_PROCESSES-1))
for i in $( seq 0 $STOP_VALUE)
do
    echo "Process $i:"
    OUT_FILE="100_5_14_25_out$i.csv"
    echo $OUT_FILE
    java -jar validator.jar 100_5_14_25.csv $OUT_FILE 
done
