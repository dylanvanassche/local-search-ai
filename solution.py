#!/usr/bin/env python3
import time
import random
import logging
from base_solution import BaseSolution


class Solution(BaseSolution):
    def __init__(self, cars, zones, requests, time_matrix, cost=None):
        super().__init__(time_matrix)
        self._name = int(time.time())
        self._cars = cars
        self._zones = zones
        self._requests = requests
        self._random = random
        self._logger = logging.getLogger(self.__class__.__name__)
        self._cost = 0

        # In case we don't supply the cost, calculate it
        for req in self._requests:
            self._cost = self._cost + req.cost

    def __str__(self):
        return "SOLUTION {}\
                \n\tcars: {}\
                \n\tzones: {}\
                \n\trequests: {}\
                \n\tcost: {}"\
                .format(self._name, self._cars, self._zones, self._requests, self._cost)

    def move_car(self, car):
        _cars = self._cars.copy()
        _zones = self._zones.copy()
        _requests = self._requests.copy()
        zone1 = car.assigned_zone
        zone2 = random.choice(_zones)

        for req1 in car.assigned_requests:
            req1.unassign()
        car.assigned_requests = []
        zone1.assigned_cars.remove(car)
        zone2.assigned_cars.append(car)
        car.assigned_zone = zone2

        random.shuffle(_requests)
        self.reassign_requests(_requests)

        # Return our changed Solution as a new Solution object
        return Solution(_cars, _zones, _requests, self._time_matrix)

    def switch_cars(self, car1, car2):
        # Keep our old solution
        _cars = self._cars.copy()
        _zones = self._zones.copy()
        _requests = self._requests.copy()

        # Perform a zone lookup
        zone1 = car1.assigned_zone
        zone2 = car2.assigned_zone

        # Unassign each request from the cars
        for req1 in car1.assigned_requests:
            req1.unassign()
        car1.assigned_requests = []

        for req2 in car2.assigned_requests:
            req2.unassign()
        car2.assigned_requests = []

        # Switch cars between zones
        zone1.assigned_cars.remove(car1)
        zone2.assigned_cars.remove(car2)
        zone1.assigned_cars.append(car2)
        car2.assigned_zone = zone1
        zone2.assigned_cars.append(car1)
        car1.assigned_zone = zone2

        # Try to assign each unassigned request in a random order
        self._random.shuffle(_requests)
        self.reassign_requests(_requests)

        # Return our changed Solution as a new Solution object
        return Solution(_cars, _zones, _requests, self._time_matrix)

    def switch_cars_advanced(self, number_of_cars, number_of_shifts):
        # Keep our old solution
        _cars = self._cars.copy()
        _zones = self._zones.copy()
        _requests = self._requests.copy()

        # Pick randomly X cars
        number_car_list = []
        while len(number_car_list) < number_of_cars:
            car = random.choice(_cars)
            while(car in number_car_list):
                car = random.choice(_cars)
            number_car_list.append(car)

        # Perform a zone lookup
        number_zone_list = []
        for car in number_car_list:
            number_zone_list.append(car.assigned_zone)

        # Unassign each request and zone from the cars
        for car in number_car_list:
            for req in car.assigned_requests:
                req.unassign()
            car.assigned_requests = []
            car.assigned_zone = None

        # Switch cars between zones with a shift of Y cars
        for i in range(len(number_zone_list)):
            number_zone_list[i].assigned_cars.remove(number_car_list[i])

        number_car_list = number_car_list[number_of_shifts::] + number_car_list[:number_of_shifts:]

        for i in range(len(number_zone_list)):
            number_zone_list[i].assigned_cars.append(number_car_list[i])
            number_car_list[i].assigned_zone = number_zone_list[i]

        # Try to assign each unassigned request in a random order
        self._random.shuffle(_requests)
        self.reassign_requests(_requests)

        # Return our changed Solution as a new Solution object
        return Solution(_cars, _zones, _requests, self._time_matrix)

    def move_request_to_other_car(self, req):
        # Keep our old solution
        _cars = self._cars.copy()
        _zones = self._zones.copy()
        _requests = self._requests.copy()

        # If request is assigned, unassign it
        if req.is_assigned():
            for car in req.allowed_cars:
                if req in car.assigned_requests:
                    car.assigned_requests.remove(req)
                    req.unassign()
                    break

        # Try to assign the request to another car
        car = self._random.choice(req.allowed_cars)
        try:
            for zone in _zones:
                if car in zone.assigned_cars:
                    is_overlapping = False
                    for assigned_req in car.assigned_requests:
                        if self._time_matrix.is_overlapping(req.name, assigned_req.name):
                            self._logger.debug("Request {} is overlapping with request {}"\
                                               .format(req.name, assigned_req.name))
                            is_overlapping = True
                            break
                    if not is_overlapping:
                        req.assign(zone)
                        car.assigned_requests.append(req)
                        self._logger.debug("Request {} successfully assigned to car {}"\
                                       .format(req.name, car.name))
                        break
        except ValueError:
            pass

        return Solution(_cars, _zones, _requests, self._time_matrix)

    def move_request_to_other_zone(self, req, new_zone):
        # Keep our old solution
        _cars = self._cars.copy()
        _zones = self._zones.copy()
        _requests = self._requests.copy()

        assigned_zone = None
        assigned_car = None
        if req.is_assigned():
            for zone in _zones:
                for car in zone.assigned_cars:
                    if req in car.assigned_requests:
                        assigned_zone = zone
                        assigned_car = car
                        break
        cars = req.requested_zone.assigned_cars
        random.shuffle(cars)
        for car in cars:
            if car in req.allowed_cars:
                is_overlapping = False
                for assigned_req in car.assigned_requests:
                    if self._time_matrix.is_overlapping(req.name, assigned_req.name):
                        self._logger.debug("Request {} is overlapping with request {}"\
                                           .format(req.name, assigned_req.name))
                        is_overlapping = True
                        break
                if not is_overlapping:
                    req.assign(new_zone)
                    car.assigned_requests.append(req)
                    self._logger.debug("Request {} successfully assigned to car {}"\
                                       .format(req.name, car.name))
                    break
        return Solution(_cars, _zones, _requests, self._time_matrix)

    @property
    def name(self):
        return self._name

    @property
    def cost(self):
        return self._cost

    @property
    def requests(self):
        return self._requests

    @property
    def zones(self):
        return self._zones

    @property
    def cars(self):
        return self._cars
