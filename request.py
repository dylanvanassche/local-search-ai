#!/usr/bin/env python3
import logging


class Request:
    def __init__(self, name, requested_zone, allowed_cars, penalty_unassigned, penalty_neighbor):
        self._name = name
        self._requested_zone = requested_zone
        self._allowed_cars = allowed_cars
        self._penalty_unassigned = int(penalty_unassigned)
        self._penalty_neighbor = int(penalty_neighbor)
        self._cost = self._penalty_unassigned  # Not assigned yet
        self._logger = logging.getLogger(self.__class__.__name__)

    def __str__(self):
        return "REQUEST {}\
                \n\trequested zone: {}\
                \n\tallowed cars: {}\
                \n\tpenalty unassigned: {}\
                \n\tpenalty neighbor: {}\
                \n\tcost: {}"\
                .format(self.name,
                        self._requested_zone,
                        self._allowed_cars,
                        self._penalty_unassigned,
                        self._penalty_neighbor,
                        self._cost)

    """
        Assign the Request to a Zone, in case the Zone is a neighbor, the `isNeighbor` flag must be set
        to True. The cost of this Request is automatically updated.

        Raises: ValueError if the zone may not be assigned to this request.
    """
    def assign(self, zone):
        # We want to assign as much as possible each request to its requested zone: cost == 0
        if zone == self._requested_zone:
            self._cost = 0
            return
        # Neighbor zones are less frequent
        elif zone in self._requested_zone.neighbours:
            self._cost = self._penalty_neighbor
            return

        # Assigning not allowed, raise exception
        raise ValueError("You can't assign a zone that's not the requested one or one of \
                         its neighbor")

    """Unassign the Request of its Zone. The cost of the Request is automatically updated."""
    def unassign(self):
        self._cost = self._penalty_unassigned

    """
        A request can be assigned if its cost is either the ASSIGNED cost or the
        NEIGHBOR ASSIGNED cost. In all other cases, the request is unassigned!
    """
    def is_assigned(self):
        return self._cost == 0 or self.cost == self._penalty_neighbor

    @property
    def name(self):
        return self._name

    @property
    def requested_zone(self):
        return self._requested_zone

    @requested_zone.setter
    def requested_zone(self, z):
        self._requested_zone = z

    @property
    def allowed_cars(self):
        return self._allowed_cars

    @allowed_cars.setter
    def allowed_cars(self, c):
        self._allowed_cars = c

    @property
    def penalty_unassigned(self):
        return self._penalty_unassigned

    @property
    def penalty_neighbor(self):
        return self._penalty_neighbor

    @property
    def cost(self):
        return self._cost
