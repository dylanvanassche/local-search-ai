#!/usr/bin/env python3
import logging
import random
from solution import Solution
from base_solution import BaseSolution


class InitialSolution(BaseSolution):
    def __init__(self, time_matrix):
        super().__init__(time_matrix)
        self._cars = None
        self._zones = None
        self._requests = None
        self._random = random
        self._logger = logging.getLogger(self.__class__.__name__)

    def generateRandomSolution(self, cars, zones, requests):
        # Create copy of input data
        _cars = cars.copy()
        _zones = zones.copy()
        _requests = requests.copy()

        # Create initial feasible random solution
        # As long as we have cars available, assign them randomly
        while len(_cars) > 0:
            # Shuffle zones randomly and add all cars
            self._random.shuffle(_zones)
            for zone in _zones:
                picked_car = self._random.choice(_cars)
                zone.assigned_cars.append(picked_car)
                picked_car.assigned_zone = zone
                _cars.remove(picked_car)

                # Exit the loop when we don't have any cars left
                if len(_cars) <= 0:
                    break

        # Debug information for cars assignment
        if self._logger.level >= logging.DEBUG:
            for zone in _zones:
                self._logger.debug("Assigned cars: {} for zone: {}".format(zone.assigned_cars, zone.name))

        # Try to assign each request to a car without overlapping
        self._random.shuffle(_requests)
        for req in _requests:
            # Pick an allowed car using Round Robin
            is_assigned = False
            for car in req.allowed_cars:
                # Check overlapping, in case no requests are assigned yet, the loop is skipped
                is_overlapping = self.check_overlap(req, car)

                # Try to assign request (zone restrictions)
                if not is_overlapping:
                    is_assigned = self.assign_request(req, car)
                    if is_assigned:
                        break

            # In case we couldn't assign the request, make sure it's unassigned
            if not is_assigned:
                req.unassign()

        # Return a Solution object which can be manipulated further
        return Solution(cars, _zones, _requests, self._time_matrix)
